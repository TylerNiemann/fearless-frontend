function createCard(title, description, pictureUrl, starts, ends, name) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${name}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer bg-transparent border-success">${starts} - ${ends}</div>
        </div>
      </div>
    `;
  }



  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        throw new Error('Error');
      } else {
        const data = await response.json();
        
        let counter = 0;
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const name = details.conference.location.name;
            const start = new Date(details.conference.starts);
            const starts = start.toLocaleDateString(undefined, {day: 'numeric', year: 'numeric', month: 'numeric'})
            const end = new Date(details.conference.ends);
            const ends = end.toLocaleDateString(undefined, {day: 'numeric', year: 'numeric', month: 'numeric'})
            const html = createCard(title, description, pictureUrl, starts, ends, name);
            const column = document.querySelector(`.col${counter}`);
            column.innerHTML += html;
            counter ++;
          }
        }
  
      }
    } catch (e) {
        console.error(e);
        const errorMessageElement = document.getElementById('error-message');
        errorMessageElement.textContent = 'An error occurred while fetching and processing the data. Please try again later.';
        errorMessageElement.classList.remove('d-none');
    }
  
  });
