
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if(response.ok){
        const data = await response.json();

        const locations_el = document.querySelector('#location');
        const locations = data.locations;
        locations.forEach(obj => {

            let option_el = document.createElement('option')
            option_el.value = obj.id;
            option_el.textContent = obj.name;
            locations_el.appendChild(option_el)

        });

        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
          event.preventDefault();
          const formData = new FormData(formTag);
          const json = JSON.stringify(Object.fromEntries(formData));
          const locationUrl = 'http://localhost:8000/api/conferences/';
          const fetchConfig = {
          method: "post",
          body: json,
          headers: {
              'Content-Type': 'application/json',
          },
          };
          const response = await fetch(locationUrl, fetchConfig);
          if (response.ok) {
          formTag.reset();
          const newLocation = await response.json();
          }          
        });
    }
    
    

  });